from machine import Pin, PWM
import time
import utime
#import lib_mov
from variables import *
import lib_mov_pwm as lmp
import lib_scan


# SETUP SERVO
servo = lib_scan.Servo()

# wait until the sensor is triggered
lmp.stop()
time.sleep(1)

print("waiting for the start sign..")
triggered = False
while not triggered:
    distance = lib_scan.distanza()
    if distance < THRES_START:
        triggered = True

print('We roll! starting the real loop')
#lib_mov.avanti_tutta()
lmp.go(durata=0, towards='FRONT', andamento=ADAGIO)
time.sleep(0.2)  # otherwise it recognizes the start as an obstacle
counter = 1
gear = 1
counter_gear = 0
verso = DIRECTION
while True:
    time.sleep(TIME_TO_WAIT)
    distance = servo.scan_step()
    # IF WE SEE AN OBSTACLE, LET'S TAKE A CLOSER LOOK
    if distance < THRES_OBSTACLE:
        print("Is this an obstacle?")
        lmp.go(durata=0, towards='FRONT', andamento=ANDANTE)
        #time.sleep(0.1)
        #distance = lib_scan.distanza()
        if 1 > 0:
            #if distance < THRES_OBSTACLE:
            #print("I think so.. slow down and let me check again!")
            #lmp.go(durata=0, towards='FRONT', andamento=ADAGIO)
            time.sleep(0.001)
            distance = lib_scan.distanza()
            if distance < THRES_FULL_SCAN:
                counter_gear = 0
                gear = 1
                free = False
                print(
                    "Now I am pretty sure that's an obstacle, let me stop and scan the area")
                lmp.stop()
                while not free:
                    print("i am blocked, need to go back")
                    closest_obstacle_index, closest_obstacle_position, _ = servo.full_scan()
                    print(
                        f"closest_obstacle_position at {closest_obstacle_position} cm")
                    if closest_obstacle_position > THRES_FULL_SCAN:
                        print("now it's free, I got myself out")
                        free = True
                    else:
                        correction = 'LEFT'
                        print(
                            f"index {closest_obstacle_index} out of {N_SCAN_STEP_FULLSCAN}")
                        if closest_obstacle_index >= (N_SCAN_STEP_FULLSCAN / 2):
                            print(
                                "ostacolo sembra piu vicino a sx, giro a dx all'indietro")
                            lmp.go(durata=0.2, andamento=MARCIA, towards="BACK")
                            lmp.turn(durata=0.2, andamento=MARCIA,
                                     direction="RIGHT", towards="BACK")
                            lmp.turn(durata=0.2, andamento=ADAGIO,
                                     direction="RIGHT", towards="BACK")
                            time.sleep(0.15)
                            correction = 'LEFT'
                        else:
                            print(
                                "ostacolo sembra piu vicino a dx, giro a sx all'indietro")
                            lmp.go(durata=0.2, andamento=MARCIA, towards="BACK")
                            lmp.turn(durata=0.2, andamento=MARCIA,
                                     direction="LEFT", towards="BACK")
                            lmp.turn(durata=0.2, andamento=ADAGIO,
                                     direction="LEFT", towards="BACK")
                            time.sleep(0.15)
                            correction = 'RIGHT'
                # HERE THE WHILE FINISHED, SO WE ARE SURE IT'S FREE AND WE CAN START
                lmp.go(durata=0.1, andamento=ADAGIO, towards="FRONT")
                lmp.turn(durata=0.15, andamento=ANDANTE,
                         towards='FRONT', direction=correction, strong=False)
                lmp.go(durata=0, andamento=ADAGIO, towards="FRONT")
            elif distance < THRES_OBSTACLE:  # conferma che la distanza rilevata e' corretta
                print("we are close but not that much, we can avoid without stopping")
                counter_gear = 0
                gear = 1
                #lib_mov.stop() #da eliminare se funziona bene
                if servo.get_duty() < SCAN_CENTER:
                    print("ostacolo a dx, giro a sn")
                    if distance > (THRES_OBSTACLE / 2):
                        lmp.turn(durata=TURNING_TIME, andamento=ADAGIO,
                                 towards='FRONT', direction="LEFT", strong=False)
                    else:
                        lmp.turn(durata=TURNING_TIME, andamento=ADAGIO,
                                 towards='FRONT', direction="LEFT", strong=True)
                    # per evitare che continui a girare per prob meccanici
                    lmp.turn(durata=TURNING_TIME/2, andamento=ANDANTE,
                             towards='FRONT', direction="RIGHT", strong=False)
                if servo.get_duty() >= SCAN_CENTER:
                    print("ostacolo a sx, giro a dx")
                    if distance > (THRES_OBSTACLE / 2):
                        lmp.turn(durata=TURNING_TIME, andamento=ADAGIO,
                                 towards='FRONT', direction="RIGHT", strong=False)
                    else:
                        lmp.turn(durata=TURNING_TIME, andamento=ADAGIO,
                                 towards='FRONT', direction="RIGHT", strong=True)
                    # per evitare che continui a girare per prob meccanici
                    lmp.turn(durata=TURNING_TIME/2, andamento=ANDANTE,
                             towards='FRONT', direction="LEFT", strong=False)
                time.sleep(0.1)
                lmp.go(durata=0, andamento=ANDANTE, towards='FRONT')
    # IF THERE IS SPACE, SPEED UP
    else:
        if gear >= 3:
            counter += 1
            # ONCE IN A WHILE; GO CRAZY AND DRIFT AROUND
            if counter % DRIFT_COUNTER == 0:
                print("yoooooooooooo driftt")
                lmp.drift(durata=3)
        counter_gear += 1
        print(counter_gear)
        if counter_gear > ENOUGH_TO_CHANGE_GEAR and gear < len(GEARS) - 2:
            counter_gear = 0
            gear += 1
            print(
                f"a bit of space, let's try going faster! period full at {GEARS[gear]/PERIOD*100}\%")

        else:
            print(
                f"waiting to gear up.. period full at {GEARS[gear]/PERIOD*100}\%")
        lmp.go(durata=0, towards='FRONT', andamento=GEARS[gear])

print("FINISHED")
