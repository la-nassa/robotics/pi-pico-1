from machine import Pin, PWM
import time
import utime
from variables import *

# SENSORE DI DISTANZA
prossi_trig = Pin(U_SENSOR_PIN_TRIGGER, Pin.OUT)  # verde
prossi_echo = Pin(U_SENSOR_PIN_ECHO, Pin.IN, Pin.PULL_DOWN)  # viola


class Servo():

    def __init__(self):
        self.servo = PWM(Pin(SERVO_PIN))  # arancio
        self.servo.freq(SERVO_FREQ)
        self.servo.duty_ns(SCAN_CENTER)
        self.step_pwm = SERVO_STEP  # old 80000
        self.verso = DIRECTION
        self.max_left = SCAN_MAX_LEFT
        self.max_right = SCAN_MAX_RIGHT
        self.step_pwm = SERVO_STEP
        self.num_steps = N_SCAN_STEP_FULLSCAN

    def get_duty(self):
        return self.servo.duty_ns()

    def straight(self):
        self.servo.duty_ns(SCAN_CENTER)

    def scan_step(self):
        current_duty = self.servo.duty_ns()
        if self.max_right < current_duty < self.max_left:
            self.servo.duty_ns(current_duty + self.step_pwm * self.verso)
            #print(f" nuovo {current_duty+step_pwm*verso}, vecchio {servo.duty_ns()}")

        if current_duty <= self.max_right:
            self.verso = 1
            new_duty = int(current_duty + 1.1 * self.step_pwm * self.verso)
            if new_duty < self.max_right:
                self.servo.duty_ns(self.max_right)
            else:
                self.servo.duty_ns(new_duty)
        elif current_duty >= self.max_left:
            self.verso = -1
            new_duty = int(current_duty + 1.1 * self.step_pwm * self.verso)
            if new_duty > self.max_left:
                self.servo.duty_ns(self.max_left)
            else:
                self.servo.duty_ns(new_duty)
            #print("2",current_duty+step_pwm*verso, verso, servo.duty_ns())
        #print(f"old duty {current_duty}, new one: {self.servo.duty_ns()}")
        return distanza()

    def full_scan(self):
        """Performs a full scan and return the index of the minimum distance to locate the closest object."""
        if self.verso == 1:
            self.servo.duty_ns(self.max_right + 1000)
        elif self.verso == -1:
            self.servo.duty_ns(self.max_left - 1000)
        servo_time = 0.03
        time.sleep(servo_time)
        list_distances = []
        for i in range(self.num_steps):
            distance = self.scan_step()
            time.sleep(servo_time)
            list_distances.append(distance)

        min_val = min(list_distances)
        min_pos = list_distances.index(min_val)
        return min_pos, min_val, list_distances


def distanza():
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    utime.sleep_us(5)  # delayMicroseconds(2);
    prossi_trig.value(1)  # digitalWrite(trigPin, HIGH);
    utime.sleep_us(10)  # delayMicroseconds(10);
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    utime.sleep_us(2)

    wait_count = 0
    while prossi_echo.value() == 0:
        StartTime = utime.ticks_us()/1000000
        wait_count = wait_count+1
        if wait_count > 5000:
            print("skip to next measurement")
            return 1000
    # save time of arrival
    StopTime = StartTime
    while prossi_echo.value() == 1:
        StopTime = utime.ticks_us()/1000000

    duration = StopTime - StartTime
    #print(duration)
    # Calculating the distance
    # Speed of sound wave divided by 2 (go and back)
    distance = duration * 34300 / 2
    #print("Distance: ", distance)
    #print(distance)
    #print(" cm")
    return distance
