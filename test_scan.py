from machine import Pin, PWM
import time
import utime
#import lib_mov
from variables import *
import lib_mov_pwm as lmp
import lib_scan


# SETUP SERVO

servo = lib_scan.Servo()  # arancio

min_i, min_val, list_d = servo.full_scan()
print(f"min_i: {min_i}, min_val: {min_val}, full: {list_d}")
