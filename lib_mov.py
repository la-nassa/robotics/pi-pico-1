from machine import Pin, PWM
import time
import utime
from variables import *

led_pico = Pin(LED_PIN, Pin.OUT)
pin_indietro_dx = Pin(MOTOR_PIN_RIGHT_BACK, Pin.OUT)
pin_avanti_dx = Pin(MOTOR_PIN_RIGHT_FRONT, Pin.OUT)
pin_avanti_sx = Pin(MOTOR_PIN_LEFT_BACK, Pin.OUT)
pin_indietro_sx = Pin(MOTOR_PIN_LEFT_FRONT, Pin.OUT)
prossi_trig = Pin(U_SENSOR_PIN_TRIGGER, Pin.OUT)  # verde
prossi_echo = Pin(U_SENSOR_PIN_ECHO, Pin.IN, Pin.PULL_DOWN)  # viola


def stop():
    # starting position
    pin_avanti_sx.value(0)
    pin_indietro_sx.value(0)
    pin_avanti_dx.value(0)
    pin_indietro_dx.value(0)


def gira_avanti_dx(durata=1):
    print(durata)
    pin_avanti_sx.value(1)
    pin_indietro_sx.value(0)
    pin_avanti_dx.value(0)
    pin_indietro_dx.value(0)
    time.sleep(durata)
    stop()


def gira_avanti_sx(durata=1):
    print(durata)
    pin_avanti_sx.value(0)
    pin_indietro_sx.value(0)
    pin_avanti_dx.value(1)
    pin_indietro_dx.value(0)
    time.sleep(durata)
    stop()


def avanti_tutta(durata=0):
    print("avanti_tutta!")
    led_pico.value(1)
    print(durata)
    pin_avanti_dx.value(1)
    pin_indietro_sx.value(0)
    pin_avanti_sx.value(1)
    pin_indietro_dx.value(0)
    if durata != 0:
        time.sleep(durata)
        stop()


def gira_indietro_sx(durata=1):
    print(durata)
    pin_avanti_sx.value(0)
    pin_indietro_sx.value(0)
    pin_avanti_dx.value(0)
    pin_indietro_dx.value(1)
    time.sleep(durata)
    stop()


def gira_indietro_dx(durata=1):
    print(durata)
    pin_avanti_sx.value(0)
    pin_indietro_sx.value(1)
    pin_avanti_dx.value(0)
    pin_indietro_dx.value(0)
    time.sleep(durata)
    stop()


def indietro_tutta(durata=0):
    print(durata)
    pin_avanti_sx.value(0)
    pin_indietro_sx.value(1)
    pin_avanti_dx.value(0)
    pin_indietro_dx.value(1)
    if durata != 0:
        time.sleep(durata)
        stop()


def distanza():
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    utime.sleep_us(5)  # delayMicroseconds(2);
    prossi_trig.value(1)  # digitalWrite(trigPin, HIGH);
    utime.sleep_us(10)  # delayMicroseconds(10);
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    utime.sleep_us(2)

    wait_count = 0
    while prossi_echo.value() == 0:
        StartTime = utime.ticks_us()/1000000
        wait_count = wait_count+1
        if wait_count > 5000:
            print("skip to next measurement")
            return 1000
    # save time of arrival
    StopTime = StartTime
    while prossi_echo.value() == 1:
        StopTime = utime.ticks_us()/1000000

    duration = StopTime - StartTime
    print(duration)
    # Calculating the distance
    # Speed of sound wave divided by 2 (go and back)
    distance = duration * 34300 / 2
    print("Distance: ")
    print(distance)
    print(" cm")
    return distance


def scan(servo, verso, estrema_dx, estrema_sn, step_pwm):
    current_duty = servo.duty_ns()
    if estrema_dx < current_duty < estrema_sn:
        servo.duty_ns(current_duty+step_pwm*verso)
        print(
            f" nuovo {current_duty+step_pwm*verso}, vecchio {servo.duty_ns()}")
    if current_duty <= estrema_dx or current_duty >= estrema_sn:
        verso = verso*(-1)
        servo.duty_ns(int(current_duty+1.1*step_pwm*verso))
        print("2", current_duty+step_pwm*verso, verso, servo.duty_ns())
    print("fine scan", verso, distanza())
    return distanza(), verso
