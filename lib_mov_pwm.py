from machine import Pin, PWM
from variables import *
import time
from array import array

####
# Using Pulse Width Modulation (PWM)
#
# We set the frequency f to be 500 Hz.
# This means we have a period of 2 ms, 2000 us, 2000000 ns
# We can set the duty cicle (how much of this period is actually used)
# So if we set it to full, the speed is the same as setting the Pin to 1
# Setting it to various amounts (between 0 and 2ms) will control the speed
# Half of the period is half the speed (is it?) and so on.
# We defined various speed in the variables.py file.
####

# LUCE
led_pico = Pin(LED_PIN, Pin.OUT)
# MOTORI
pin_indietro_dx = PWM(Pin(MOTOR_PIN_RIGHT_BACK))
pin_indietro_dx.freq(PWM_MOTOR_FREQ)
pin_indietro_dx.duty_ns(STOP)
pin_avanti_dx = PWM(Pin(MOTOR_PIN_RIGHT_FRONT))
pin_avanti_dx.freq(PWM_MOTOR_FREQ)
pin_avanti_dx.duty_ns(STOP)
pin_indietro_sx = PWM(Pin(MOTOR_PIN_LEFT_BACK))
pin_indietro_sx.freq(PWM_MOTOR_FREQ)
pin_indietro_sx.duty_ns(STOP)
pin_avanti_sx = PWM(Pin(MOTOR_PIN_LEFT_FRONT))
pin_avanti_sx.freq(PWM_MOTOR_FREQ)
pin_avanti_sx.duty_ns(STOP)


def stop():
    """Stop the motors. (Setting the duty cycle to be 0%)"""
    # stop the motors
    pin_avanti_sx.duty_ns(STOP)
    pin_indietro_sx.duty_ns(STOP)
    pin_avanti_dx.duty_ns(STOP)
    pin_indietro_dx.duty_ns(STOP)
    led_pico.value(DOWN)


def forward(andamento=ANDANTE):
    """Set the motors to go forward (forever, you may need to call stop() after)."""
    pin_avanti_dx.duty_ns(int(andamento[1]))
    pin_avanti_sx.duty_ns(int(andamento[0]))
    pin_indietro_dx.duty_ns(STOP)
    pin_indietro_sx.duty_ns(STOP)


def backward(andamento=ANDANTE):
    """Set the motors to go backward (forever, you may need to call stop() after)."""
    pin_indietro_dx.duty_ns(int(andamento[0]))
    pin_indietro_sx.duty_ns(int(andamento[1]))
    pin_avanti_dx.duty_ns(STOP)
    pin_avanti_sx.duty_ns(STOP)


def left(andamento=ANDANTE, towards='FRONT', strong=False):
    """
    Turn left going backwards or forwards
    (forever, you may need to call stop() after).
    if strong is true, it turns faster by using both sides.
    """
    # TURNING BACK AND LEFT
    if towards == 'BACK':
        pin_indietro_dx.duty_ns(STOP)
        if strong:
            pin_avanti_sx.duty_ns(int(andamento[0]))
        else:  # how == "SWEET"
            pin_avanti_sx.duty_ns(STOP)
        pin_indietro_dx.duty_ns(int(andamento[1]))
        pin_avanti_sx.duty_ns(STOP)
    # TURNING FRONT AND LEFT
    else:  # towards == 'FRONT':
        pin_indietro_dx.duty_ns(STOP)
        if strong:
            pin_indietro_sx.duty_ns(int(andamento[0]))
        else:  # how == "SWEET"
            pin_indietro_sx.duty_ns(STOP)
        pin_avanti_dx.duty_ns(int(andamento[1]))
        pin_avanti_sx.duty_ns(STOP)


def right(andamento=ANDANTE, towards='FRONT', strong=False):
    """
    Turn right going backwards or forwards
    (forever, you may need to call stop() after).
    if strong is true, it turns faster by using both sides.
    """
    # TURNING BACK AND RIGHT
    if towards == 'BACK':
        pin_indietro_dx.duty_ns(STOP)
        if strong:
            pin_avanti_dx.duty_ns(int(andamento[1]))
        else:  # how == "SWEET"
            pin_avanti_dx.duty_ns(STOP)
        pin_indietro_sx.duty_ns(int(andamento[0]))
        pin_avanti_sx.duty_ns(STOP)
    # TURNING FRONT AND RIGHT
    else:  # if towards == 'FRONT':
        pin_indietro_sx.duty_ns(STOP)
        if strong:
            pin_indietro_dx.duty_ns(int(andamento[1]))
        else:  # how == "SWEET"
            pin_indietro_dx.duty_ns(STOP)
        pin_avanti_sx.duty_ns(int(andamento[0]))
        pin_avanti_dx.duty_ns(STOP)


def go(durata=1, andamento=ANDANTE, towards='FRONT'):
    """
    Wrapper for moving the robot forward or backwards.
    You can choose length, speed and direction.
    """
    led_pico.value(UP)
    if towards == 'FRONT':
        forward(andamento=andamento)
    elif towards == 'BACK':
        backward(andamento=andamento)
    else:
        print("I know only how to towards=FRONT or BACK, sorry")
        stop()

    if durata != 0:
        time.sleep(durata)
        stop()


def turn(durata=1, andamento=ANDANTE, towards='FRONT', direction="LEFT", strong=False):
    """
    Wrapper for turning the robot left or right.
    You can choose length, speed and direction of the turn.
    """
    led_pico.value(DOWN)
    if direction == 'LEFT':
        left(andamento=andamento, towards=towards, strong=strong)
    elif direction == 'RIGHT':
        right(andamento=andamento, towards=towards, strong=strong)
    else:
        print("I know only to turn in direction=LEFT or RIGHT, sorry")
        stop()

    if durata != 0:
        time.sleep(durata)
        stop()


def drift(durata=1, direction="LEFT"):
    """Drift around shooting both engines at maximum power in 2 different directions"""
    led_pico.value(UP)
    pin_indietro_dx.duty_ns(STOP)
    pin_indietro_sx.duty_ns(PRESTO)
    pin_avanti_dx.duty_ns(PRESTO)
    pin_avanti_sx.duty_ns(STOP)
    if durata != 0:
        time.sleep(durata)
        stop()
